# OpenML dataset: Film_review

https://www.openml.org/d/42393

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

wswdwd

## Contributing

This is a read-only mirror of an [OpenML dataset](https://www.openml.org/d/42393). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42393/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42393/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42393/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

